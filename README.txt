Description
-----------
This module use to adds the image in block on your Drupal site.

Requirements
------------
Drupal 7.x

Installation
------------
1. Copy the entire imageblock directory the Drupal sites/all/modules directory.

2. Login as an administrator. Enable the module in the "Administer" -> "Modules"

4. Assign the block on following url admin/structure/block.

6. Click on configure to added the image on url admin/structure/block/manage/imageblock/imageblock/configure 


